package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.text.MessageFormat;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/api")
public class MainController {

    @GetMapping("/deploy")
    public String index() {
        return MessageFormat.format("Web deploy ok: {0}", LocalDateTime.now());
    }
}
